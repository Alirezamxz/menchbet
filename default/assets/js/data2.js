

	oddFormat = function(n) {
		n = parseFloat(n).formatMoney(2);
		if(direction=="rtl") return toRtlNumber(n);
		return n;
	}

	timeFormat = function(n) {
		if(direction=="rtl") return toRtlNumber(n);
		return n;
	}

	scoreFormat = function(n) {
		if(direction=="rtl") return toRtlNumber(n);
		return n;
	}

	numberFormat = function(n) {
		if(direction=="rtl") return toRtlNumber(n);
		return n;
	}

	moneyFormat = function(n) {
		n = parseFloat(n).formatMoney();
		if(direction=="rtl") return toRtlNumber(n);
		return n;
	}

	moneyFormatLtr = function(n) {
		n = parseFloat(n).formatMoney();
		return n;
	}

	toRtlNumber = function(n) {
		if(typeof(pers_num)!="undefined" && !pers_num) return n;
		n = n + "";
		n = n.replace(new RegExp("\\.", 'g'), unescape(","));
		n = n.replace(new RegExp("0", 'g'), unescape("%u06F0"));
		n = n.replace(new RegExp("1", 'g'), unescape("%u06F1"));
		n = n.replace(new RegExp("2", 'g'), unescape("%u06F2"));
		n = n.replace(new RegExp("3", 'g'), unescape("%u06F3"));
		n = n.replace(new RegExp("4", 'g'), unescape("%u06F4"));
		n = n.replace(new RegExp("5", 'g'), unescape("%u06F5"));
		n = n.replace(new RegExp("6", 'g'), unescape("%u06F6"));
		n = n.replace(new RegExp("7", 'g'), unescape("%u06F7"));
		n = n.replace(new RegExp("8", 'g'), unescape("%u06F8"));
		n = n.replace(new RegExp("9", 'g'), unescape("%u06F9"));
		return n;
	}

	odd_selected = function(item, d, p, on) {

		var d = d.split("x");

		var data = new Object();

		data.id = new Date().getTime();
		data.event_id = d[0];
		data.outcome_id = d[1];
		data.odd_id = d[2];
		data.price = p;

		data.home_team_name = $(".event-"+data.event_id+" .home-team").length>0 ? $(".event-"+data.event_id+" .home-team").html() : $(".home-team").html();
		data.away_team_name = $(".event-"+data.event_id+" .home-team").length>0 ? $(".event-"+data.event_id+" .away-team").html() : $(".away-team").html();
		
		data.outcome_name = $(".market-type-"+data.outcome_id+" .market-name").html();
		if(data.outcome_name==undefined) data.outcome_name = findKeyword("result");

		data.odd_name = on;

		add_to_list(data);

	}

	var cloud_socket = function(address) {

		this.address = address;
		this.connection;

		this.connect = function(onData,onConnect,onClose) {
			
			if(this.connection!=null) {
				this.connection.close();
				this.connection = null;
			}

			this.connection = new WebSocket(this.address);

			this.connection.onopen = function(e) {
				if(onConnect!=null) onConnect();
			}

			this.connection.onmessage = function(e) {
				if(onData!=null) onData(e.data);
			} 

			this.connection.onclose = function(e) {
				if(onClose!=null) onClose();
			}

			this.connection.onerror = function(e) {
				if(onClose!=null) onClose();
			}

		}

		this.disconnect = function() {
			if(this.connection!=null) {
				this.connection.close();
				this.connection = null;
			}
		}

		this.send = function(data) {
			if(this.connection!=null && this.connection.readyState==1) {
				this.connection.send(JSON.stringify(data));
			}
		}

	}

	var socket = null;
	var langKeys = null;

	var team_name_markets = null;
	var market_priority = null;

	findKeyword = function(k) {
		var k2 = k.toLowerCase();
		if(langKeys==null || langKeys[k2]==null) return numberFormat(k);
		return numberFormat(langKeys[k2]);
	}

	$(document).ready(function() {

		var stn = $("#subscribe-team-names").val();
		if(stn!=undefined) team_name_markets = stn.split(',');

		var smp = $("#subscribe-market-priority").val();
		if(smp!=undefined) market_priority = smp.split(',');

		$.get(typeof(link_set)=="object"&&link_set.language!=null?link_set.language:"/api/sport/data/language", function(response) {
			var json = typeof response == "object" ? response : JSON.parse(response);
			if(json.result==null || json.result!="ok") {
				//console.log("reload-place-1");
				//location.reload();
				return false;
			}
			//if(json.data!=null) langKeys = json.data;
			langKeys = [];
			if(json.data!=null) {
				for(ijy in json.data) {
					var ijy2 = ijy.toLowerCase();
					langKeys[ijy2] = json.data[ijy];
				}
			}
			event_subscription();
		});

		setInterval(function() {
			calculateTimes();
		}, 1000);
		
	});

	event_subscription = function() {

		var address = $("#cloud-server").val();
		if(address==undefined || address=="") return false;

		var sevents = $("#subscribe-events").val();
		if(sevents==undefined || sevents=="") return false;

		var stype = $("#subscribe-type").val();
		if(stype==undefined || stype=="") return false;

		var sfields = $("#subscribe-fields").val();
		if(sfields==undefined || sfields=="") return false;

		var sses = $("#subscribe-session").val();

		address = getconnectionurl(address, false);

		socket = new cloud_socket(address);
		socket.connect(function(data) {
			var json = JSON.parse(data);
			subscription_data(json);

		}, function() {

			var sj = new Object({command:"subscribe",events:sevents.split(","),fields:sfields.split(","),type:stype,session:sses,zone:versionid});

			var soutcomes = $("#subscribe-outcomes").val();
			if(soutcomes!=undefined && soutcomes!="") {
				var po = atob(soutcomes);
				sj = new Object({command:"subscribe",events:sevents.split(","),fields:sfields.split(","),type:stype,markets:JSON.parse(po),session:sses,zone:versionid});
			}
				
			socket.send(sj);

		}, function() {
			setTimeout(function() {
				//alert("Connection failed");
				getconnectionurl("", true);
				console.log("reload-place-2");
				location.reload();
			}, 2000);
		});

	}

	subscription_data = function(data) {

		if(data.command==null) return false;
		
		var command = data.command;
		
		if(command=="event") update_event(data);
		if(command=="stats") event_changed(data);
		if(command=="remove") event_removed(data);
		if(command=="markets") markets_changed(data);
		
		bind_clicks();

		calculateTimes();

	}

	bind_clicks = function() {

		$(".odd-link").unbind("click");
		$(".odd-link").click(function() {
			var d = $(this).attr("data");
			if(d==undefined || d==null) return false;
			var b = $(this).hasClass("passive") || $(this).hasClass("passive-ev") || $(this).hasClass("passive-ma");
			var c = $(this).hasClass("passive-link") || $(this).hasClass("passive-link-ev") || $(this).hasClass("passive-link-ma");
			var p = $(this).attr("price");
			if(p==undefined || p==null) return false;
			var on = $(this).attr("oname");
			if(on==undefined || on==null) return false;
			if(b==false && c==false) odd_selected(this, d, p, on);
		});

	}

	update_event = function(data) {

		var spage = $("#subscribe-page").val();
		var details_page = (spage!=undefined && spage=="details") ? true : false;
		
		var sport = $("#subscribe-sport").val();

		for(i in data.events) {
			var e = data.events[i];
			if(e.status=="not_found") {
				$(".event-"+e.id).remove();
				$(".event-type").each(function() {
					if($(this).find(".event-row").length==0) $(this).remove();
				});
				$(".score-type").each(function() {
					if($(this).find(".score-row").length==0) $(this).remove();
				});
				continue;
			}

			for(j in e.markets) {
				if(e.markets[j].outcomes!=null) {
					for(k in e.markets[j].outcomes) {
						if(e.markets[j].outcomes[k].name!=null) {
							e.markets[j].outcomes[k].name = fix_outcome_name(e.markets[j].outcomes[k].name);
						}
					}	
				}
				if(e.markets[j].odds!=null) {
					for(k in e.markets[j].odds) {
						if(e.markets[j].odds[k].name!=null) {
							e.markets[j].odds[k].name = fix_outcome_name(e.markets[j].odds[k].name);
						}
					}	
				}
			}

			var event_id = e.id;
			var event_info = e.info!=null?e.info:null;
			var event_stats = e.stats!=null?e.stats:null;
			var event_suspended = e.info!=null&&e.info.suspended!=null?e.info.suspended:true;

			if(details_page) setEventDetails(e);

			var outcomes_finded = false;
			for(j in e.markets) {
				if(inArray2(e.markets[j].id,market_priority,e.pid)) {
					var odds = e.markets[j].outcomes!=null?e.markets[j].outcomes:e.markets[j].odds;
					var odd_1 = null;
					var odd_2 = null;
					var odd_x = null;
					for(k in odds) {
						if(odds[k].name=="1") odd_1 = odds[k];
						if(odds[k].name=="2") odd_2 = odds[k];
						if(odds[k].name=="x") odd_x = odds[k];
					}
					var result = '<div class="market-box-'+e.markets[j].id+'">';
					if(odd_1!=null) {
						//odd_1.name = $(".event-"+e.id+" .home-team").html();
						odd_1.name = findKeyword("home");
						result = result + '<a href="javascript:;"  data="' + e.id + 'x' + e.markets[j].id + 'x' + odd_1.id + '" price="' + odd_1.odds + '" oname="'+odd_1.name+'" class="odd-rate odd-main-button odd-link odd-' + odd_1.id + '">' + oddFormat(odd_1.odds) + '</a>';
					} else {
						result = result + '<a href="javascript:;" class="odd-rate passive">...</a>';
					}
					if(odd_x!=null) {
						result = result + '<a href="javascript:;"  data="' + e.id + 'x' + e.markets[j].id + 'x' + odd_x.id + '" price="' + odd_x.odds + '" oname="'+odd_x.name+'" class="odd-rate odd-main-button odd-link odd-' + odd_x.id + '">' + oddFormat(odd_x.odds) + '</a>';
					} else {
						result = result + '<a href="javascript:;" class="odd-rate passive">...</a>';
					}
					if(odd_2!=null) {
						//odd_2.name = $(".event-"+e.id+" .away-team").html();
						odd_2.name = findKeyword("away");
						result = result + '<a href="javascript:;" data="' + e.id + 'x' + e.markets[j].id + 'x' + odd_2.id + '" price="' + odd_2.odds + '" oname="'+odd_2.name+'" class="odd-rate odd-main-button odd-link odd-' + odd_2.id + '">' + oddFormat(odd_2.odds) + '</a>';
					} else {

						result = result + '<a href="javascript:;" class="odd-rate passive">...</a>';
					}
					result = result + '</div>';

					$(".event-"+e.id+" .event-odds").html(result);
					outcomes_finded = true;
					break;
				}
			}

			if(!outcomes_finded) {
				var result = '<a href="javascript:;" class="odd-rate passive">...</a><a href="javascript:;" class="odd-rate passive">...</a><a href="javascript:;" class="odd-rate passive">...</a>';
				$(".event-"+e.id+" .event-odds").html(result);
			}

			for(j in e.markets) {
				var suspended = false;
				var market_id = e.markets[j].id;
				//var market_suspended = e.markets[j].suspended;
				var market_suspended = false;
				var o = e.markets[j];
				var all_odds_outcomes = o.outcomes!=null?o.outcomes:o.odds;
				for(k in all_odds_outcomes) {
					var odd = all_odds_outcomes[k];
					if(odd.status!='open') suspended = true;
					$(".odd-"+odd.id).html(oddFormat(odd.odds));

					if(odd.status=='open') {
						$(".odd-"+odd.id).removeClass("passive");
						$(".odd-link-"+odd.id).removeClass("passive-link");
					} else {
						$(".odd-"+odd.id).addClass("passive");
						$(".odd-link-"+odd.id).addClass("passive-link");
					}
				}
				if(market_suspended) {
					$(".event-"+event_id+" .market-box-"+market_id+" .odd-main-button").addClass("passive-ma");
					$(".event-"+event_id+" .market-box-"+market_id+" .odd-sub-button").addClass("passive-link-ma");
				} else {
					$(".event-"+event_id+" .market-box-"+market_id+" .odd-main-button").removeClass("passive-ma");
					$(".event-"+event_id+" .market-box-"+market_id+" .odd-sub-button").removeClass("passive-link-ma");
				}
			}

			if(event_suspended) {
				$(".event-"+event_id+" .odd-main-button").addClass("passive-ev");
				$(".event-"+event_id+" .odd-sub-button").addClass("passive-link-ev");
			} else {
				$(".event-"+event_id+" .odd-main-button").removeClass("passive-ev");
				$(".event-"+event_id+" .odd-sub-button").removeClass("passive-link-ev");
			}
			
			if(event_info!=null || event_stats!=null) {
				if(setEventTime(event_id, event_info, event_stats) && details_page) {
					$(".time .period").hide();
				}
			}

			if(event_info!=null || event_stats!=null) {
				var scores = get_score(event_info, event_stats);
				$(".event-"+e.id+" .home-score").html(timeFormat(scores.home));
				$(".event-"+e.id+" .away-score").html(timeFormat(scores.away));

				if(details_page)
				{
					if(sport=="1")
					{
						$(".home-score").html(timeFormat(scores.home));
						$(".away-score").html(timeFormat(scores.away));

						$(".home-cards").html("");
						$(".away-cards").html("");

						var stats = get_stats(event_info, event_stats);
						for(ci=0; ci<stats.home.yellow; ci++) $(".home-cards").append('<div class="card mt5"></div>'+"\n");
						for(ci=0; ci<stats.home.red; ci++) $(".home-cards").append('<div class="red card mt5"></div>'+"\n");
						for(ci=0; ci<stats.away.red; ci++) $(".away-cards").append('<div class="red card mt5"></div>'+"\n");
						for(ci=0; ci<stats.away.yellow; ci++) $(".away-cards").append('<div class="card mt5"></div>'+"\n");						
					}
					else
					{
						var scores = get_full_score(event_info, event_stats);
						if(scores["0"]!=null)
						{
							$(".home-score-0").html(scores["0"].home);
							$(".away-score-0").html(scores["0"].away);
						}
						if(scores["1"]!=null)
						{
							$(".home-score-1").html(scores["1"].home);
							$(".away-score-1").html(scores["1"].away);
						}
						if(scores["2"]!=null)
						{
							$(".home-score-2").html(scores["2"].home);
							$(".away-score-2").html(scores["2"].away);
						}
						if(scores["3"]!=null)
						{
							$(".home-score-3").html(scores["3"].home);
							$(".away-score-3").html(scores["3"].away);
						}
						if(scores["4"]!=null)
						{
							$(".home-score-4").html(scores["4"].home);
							$(".away-score-4").html(scores["4"].away);
						}
					}

				}

			}

		}

		bet_selected_marker();

	}

	event_changed = function(data) {

		var spage = $("#subscribe-page").val();
		var details_page = (spage!=undefined && spage=="details") ? true : false;
		
		var sport = $("#subscribe-sport").val();

		var event_id = data.event_id;
		var event_provider = data.pid;
		var event_info = data.info!=null?data.info:null;
		var event_stats = data!=null&&data.stats!=null&&data.stats!=null?data.stats:null;

		if(event_info!=null || event_stats!=null) {
			if(setEventTime(event_id, event_info, event_stats) && details_page) {
				$(".time .period").hide();
			}

		}

		if(event_info!=null && event_stats!=null) {

			var scores = get_score(event_info, event_stats);
			$(".event-"+event_id+" .home-score").html(timeFormat(scores.home));
			$(".event-"+event_id+" .away-score").html(timeFormat(scores.away));
				
			if(details_page)
			{
				if(sport=="1")
				{
					$(".home-score").html(timeFormat(scores.home));
					$(".away-score").html(timeFormat(scores.away));

					$(".home-cards").html("");
					$(".away-cards").html("");

					var stats = get_stats(event_info, event_stats);
					for(ci=0; ci<stats.home.yellow; ci++) $(".home-cards").append('<div class="card mt5"></div>'+"\n");
					for(ci=0; ci<stats.home.red; ci++) $(".home-cards").append('<div class="red card mt5"></div>'+"\n");
					for(ci=0; ci<stats.away.red; ci++) $(".away-cards").append('<div class="red card mt5"></div>'+"\n");
					for(ci=0; ci<stats.away.yellow; ci++) $(".away-cards").append('<div class="card mt5"></div>'+"\n");						
				}
				else
				{
					var scores = get_full_score(event_info, event_stats);
					if(scores["0"]!=null)
					{
						$(".home-score-0").html(scores["0"].home);
						$(".away-score-0").html(scores["0"].away);
					}
					if(scores["1"]!=null)
					{
						$(".home-score-1").html(scores["1"].home);
						$(".away-score-1").html(scores["1"].away);
					}
					if(scores["2"]!=null)
					{
						$(".home-score-2").html(scores["2"].home);
						$(".away-score-2").html(scores["2"].away);
					}
					if(scores["3"]!=null)
					{
						$(".home-score-3").html(scores["3"].home);
						$(".away-score-3").html(scores["3"].away);
					}
					if(scores["4"]!=null)
					{
						$(".home-score-4").html(scores["4"].home);
						$(".away-score-4").html(scores["4"].away);
					}
				}

			}

		}	

	}

	event_removed = function(data) {

		$(".event-"+data.event_id).fadeOut("slow", function() {
			$(".event-"+data.event_id).remove();
			setTimeout(function() {
				$(".event-type").each(function() {
					if($(this).find(".event-row").length==0) $(this).remove();
				});
			}, 3000);
		});

	}

	markets_changed = function(data) {

		var spage = $("#subscribe-page").val();
		var details_page = (spage!=undefined && spage=="details") ? true : false;

		var event_id = data.event;
		var pid = data.pid;
		var suspended = false;

		var new_markets = [];

		for(j in data.markets) {
			if(data.markets[j].outcomes!=null) {
				for(k in data.markets[j].outcomes) {
					if(data.markets[j].outcomes[k].name!=null) {
						data.markets[j].outcomes[k].name = fix_outcome_name(data.markets[j].outcomes[k].name);
					}
				}	
			}
			if(data.markets[j].odds!=null) {
				for(k in data.markets[j].odds) {
					if(data.markets[j].odds[k].name!=null) {
						data.markets[j].odds[k].name = fix_outcome_name(data.markets[j].odds[k].name);
					}
				}	
			}
		}

		for(mi in data.markets) {

			var new_odds = [];

			var market_id = data.markets[mi].id;
			var market_suspended = data.markets[mi].suspended;

			if(market_suspended) {
				$(".event-"+event_id+" .market-box-"+market_id+" .odd-main-button").addClass("passive-ma");
				$(".event-"+event_id+" .market-box-"+market_id+" .odd-sub-button").addClass("passive-link-ma");
			} else {
				$(".event-"+event_id+" .market-box-"+market_id+" .odd-main-button").removeClass("passive-ma");
				$(".event-"+event_id+" .market-box-"+market_id+" .odd-sub-button").removeClass("passive-link-ma");
			}

			for(i in data.markets[mi].odds) {

				var odds = data.markets[mi].odds[i];

				if(odds.state=="new") {
					if($(".event-"+event_id+" .market-box-"+market_id).length>0) {
						var obj = {"id":odds.id,"outcome":odds.outcome,"odds":odds.odds,"status":odds.status,"type":odds.type,"update":0};
						if(odds.balance!=null) obj.most_balanced = odds.balance;
						if(odds.specifiers!=null) obj.specifiers = odds.specifiers;
						if(odds.name!=null) obj.name = odds.name;
						new_odds.push(obj);
					}

				} else if(odds.state=="update") {
					
					if(odds.ps=="same") {
						
						var ex_odd = $(".odd-"+odds.id).html();
						var caret  = "";

						if(ex_odd!=undefined && ex_odd.indexOf("caret-up")>0) caret = ' <i class="fa fa-caret-up green-arrow"></i>';
						if(ex_odd!=undefined && ex_odd.indexOf("caret-down")>0) caret = ' <i class="fa fa-caret-down red-arrow"></i>';

						$(".odd-"+odds.id).html(oddFormat(odds.odds)+caret);
					
					} else if(odds.ps=="higher") {
						$(".odd-"+odds.id).html(oddFormat(odds.odds) + ' <i class="fa fa-caret-up green-arrow"></i>');
						add_odd_effect(".odd-"+odds.id, "green");
					
					} else if(odds.ps=="lower") {
						$(".odd-"+odds.id).html(oddFormat(odds.odds) + ' <i class="fa fa-caret-down red-arrow"></i>');
						add_odd_effect(".odd-"+odds.id, "red");

					}

					if(odds.status=='open') {
						$(".odd-"+odds.id).removeClass("passive");
						$(".odd-link-"+odds.id).removeClass("passive-link");
					} else {
						$(".odd-"+odds.id).addClass("passive");
						$(".odd-link-"+odds.id).addClass("passive-link");
					}

					$(".odd-"+odds.id).attr({price:odds.odds});
					$(".odd-link-"+odds.id).attr({price:odds.odds});
					
				} else if(odds.state=="remove") {
					if(details_page) {
						$(".odd-"+odds.id).remove();
						$(".odd-link-"+odds.id).remove();
					} else {
						$(".odd-"+odds.id).addClass("passive");
					}
					
				}

			}

			if(new_odds.length>0) new_markets.push({"id":market_id,"suspended":market_suspended,"odds":new_odds});

		}

		// update markets
		if(new_markets.length>0) editMarkets(details_page, event_id, new_markets, pid);

	}

	editMarkets = function(details_page, event_id, new_markets, pid) {

		if(details_page) new_markets = sortOutcomes(new_markets,pid);

		for(j in new_markets) {

			var market_id = new_markets[j].id;
			var market_suspended = new_markets[j].suspended;

			if($(".event-"+event_id+" .market-box-"+new_markets[j].id).length==0) continue;

			if(details_page) {

				var odds = new_markets[i].odds;

				var total_odds = odds.length;
				if(total_odds==0) continue;

				var col_type = "";
				var frst_col = [];
				var scnd_col = [];
				var thrd_col = [];

				// Unique odd types
				var unique_type = 0;
				var unique_names = [];
				for(j in odds) {
					var finded = false;
					for(k in unique_names) {
						if(odds[j].name==unique_names[k]) {
							finded = true;
							break;
						}
					}
					if(finded==false) {
						unique_names.push(odds[j].name);
						unique_type += 1;
					}
				}

				col_type = "odd-triple";
				if(total_odds<3 || (total_odds%2==0 && total_odds%3!=0)) col_type = "odd-double";

				if(unique_type==2) col_type = "odd-double";
				if(unique_type==3) col_type = "odd-triple";

				if(col_type == "odd-double") {

					for(j in odds) {
						if(j<(total_odds/2)) frst_col.push(odds[j]);
						else scnd_col.push(odds[j]);
					}

				} else {

					for(j in odds) {
						if(j<(total_odds/3)) {
							frst_col.push(odds[j]);
						} else {
							if(j<((total_odds/3)*2)) {
								scnd_col.push(odds[j]);
							} else {
								thrd_col.push(odds[j]);
							}
						}
					}				

				}

				var d = '';

				var max = frst_col.length > scnd_col.length ? frst_col.length : scnd_col.length;
				max = thrd_col.length > max ? thrd_col.length : max;

				if(max>0) {
					for(j=0; j<max; j++) {
						for(x=0; x<3; x++) {

							var odd = null;

							if(x==0 && frst_col.length>=(j-1)) odd = frst_col[j];
							if(x==1 && scnd_col.length>=(j-1)) odd = scnd_col[j];
							if(x==2 && thrd_col.length>=(j-1)) odd = thrd_col[j];

							if(odd==null) continue;

							// 1-Under, 2-over fixing
							/*
							if(pid==1 && odd.type!=null && odd.type=="live" && market_id==21) {
								if(odd.odd!=null && odd.odd==17) odd.name = 'under';
								else if(odd.odd!=null && odd.odd==18) odd.name = 'over';
							}
							*/

							if(odd.specifiers!=null && odd.specifiers!="") {
								odd.specifiers = fix_specifiers_name(odd.specifiers);
							}

							var oname = findKeyword(odd.name);
							if(odd.specifiers!=null && odd.specifiers!="" && odd.specifiers!="-1") oname = oname + " " + numberFormat(odd.specifiers);

							d = d + '<a href="javascript:;" class="odd-link odd-sub-button odd-link-'+odd.id+' ' + col_type + '" data="'+event_id+'x'+market_id+'x'+odd.id+'" price="' + odd.odds + '" oname="' + oname + '">';
							d = d + '<div class="odd-title"><span>'+findKeyword(odd.name)+'</span>';

							if(odd.specifiers!=null && odd.specifiers!="" && odd.specifiers!="-1")
							{
								d = d + '<span class="odd-line">'+numberFormat(odd.specifiers)+'</span>';
							}

							d = d + '</div>';
							d = d + '<div class="odd-rate odd-main-button odd-'+odd.id+'">'+oddFormat(odd.odds)+'</div>';
							d = d + '</a>';
						}
					}
				}

				d = d + '<div class="clear"></div>';
				$(".market-box-"+market_id).append(d);

			} else {

				if(inArray2(new_markets[j].id,market_priority,pid)) {
					var odds = new_markets[j].odds;
					var odd_1 = null;
					var odd_2 = null;
					var odd_x = null;

					for(k in odds) {
						if(odds[k].name=="1") odd_1 = odds[k];
						if(odds[k].name=="2") odd_2 = odds[k];
						if(odds[k].name=="x") odd_x = odds[k];
					}
					var result = '<div class="market-box-'+new_markets[j].id+'">';
					if(odd_1!=null) {
						//odd_1.name = $(".event-"+event_id+" .home-team").html();
						odd_1.name = findKeyword("home");
						result = result + '<a href="javascript:;"  data="' + event_id + 'x' + new_markets[j].id + 'x' + odd_1.id + '" price="' + odd_1.odds + '" oname="'+odd_1.name+'" class="odd-rate odd-main-button odd-link odd-' + odd_1.id + '">' + oddFormat(odd_1.odds) + '</a>';
					} else {
						result = result + '<a href="javascript:;" class="odd-rate passive">...</a>';
					}
					if(odd_x!=null) {
						result = result + '<a href="javascript:;"  data="' + event_id + 'x' + new_markets[j].id + 'x' + odd_x.id + '" price="' + odd_x.odds + '" oname="'+odd_x.name+'" class="odd-rate odd-main-button odd-link odd-' + odd_x.id + '">' + oddFormat(odd_x.odds) + '</a>';
					} else {
						result = result + '<a href="javascript:;" class="odd-rate passive">...</a>';
					}
					if(odd_2!=null) {
						//odd_2.name = $(".event-"+event_id+" .away-team").html();
						odd_2.name = findKeyword("away");
						result = result + '<a href="javascript:;" data="' + event_id + 'x' + new_markets[j].id + 'x' + odd_2.id + '" price="' + odd_2.odds + '" oname="'+odd_2.name+'" class="odd-rate odd-main-button odd-link odd-' + odd_2.id + '">' + oddFormat(odd_2.odds) + '</a>';
					} else {

						result = result + '<a href="javascript:;" class="odd-rate passive">...</a>';
					}
					result = result + '</div>';

					$(".event-"+event_id+" .event-odds").html(result);

					if(odd_1!=null) {
						if(odd_1.status=='open') {
							$(".odd-"+odd_1.id).removeClass("passive");
							$(".odd-link-"+odd_1.id).removeClass("passive-link");
						} else {
							$(".odd-"+odd_1.id).addClass("passive");
							$(".odd-link-"+odd_1.id).addClass("passive-link");
						}
					}

					if(odd_x!=null) {
						if(odd_x.status=='open') {
							$(".odd-"+odd_x.id).removeClass("passive");
							$(".odd-link-"+odd_x.id).removeClass("passive-link");
						} else {
							$(".odd-"+odd_x.id).addClass("passive");
							$(".odd-link-"+odd_x.id).addClass("passive-link");
						}
					}

					if(odd_2!=null) {
						if(odd_2.status=='open') {
							$(".odd-"+odd_2.id).removeClass("passive");
							$(".odd-link-"+odd_2.id).removeClass("passive-link");
						} else {
							$(".odd-"+odd_2.id).addClass("passive");
							$(".odd-link-"+odd_2.id).addClass("passive-link");
						}
					}

				}

			}

			if(market_suspended) {
				$(".event-"+event_id+" .market-box-"+market_id+" .odd-main-button").addClass("passive-ma");
				$(".event-"+event_id+" .market-box-"+market_id+" .odd-sub-button").addClass("passive-link-ma");
			} else {
				$(".event-"+event_id+" .market-box-"+market_id+" .odd-main-button").removeClass("passive-ma");
				$(".event-"+event_id+" .market-box-"+market_id+" .odd-sub-button").removeClass("passive-link-ma");
			}			

		}

	}

	setEventDetails = function(e) {
		
		var outcome_titles = "";
		var update_remove_timeout = 300000; // was 300000

		e.markets = sortOutcomes(e.markets,e.pid);

		for(i in e.markets) {

			var market_id = e.markets[i].id;
			//var market_suspended = e.markets[i].suspended;
			var market_suspended = false;

			var odds = e.markets[i].outcomes;

			// Remove passive odds
			var new_odds = [];
			for(j in odds) {
				//if(inArray2(e.markets[i].id,market_priority,e.pid)) {
					if(odds[j].status!='open' && parseInt(odds[j].update)>update_remove_timeout) continue;	
				//}
				new_odds.push(odds[j]);
			}
			odds = new_odds;

			var total_odds = odds.length;

			if(total_odds==0) continue;
			
			var col_type = "";
			var frst_col = [];
			var scnd_col = [];
			var thrd_col = [];

			// Unique odd types
			var unique_type = 0;
			var unique_names = [];
			for(j in odds) {
				var finded = false;
				for(k in unique_names) {
					if(odds[j].name==unique_names[k]) {
						finded = true;
						break;
					}
				}
				if(finded==false) {
					unique_names.push(odds[j].name);
					unique_type += 1;
				}
			}

			col_type = "odd-triple";
			if(total_odds<3 || (total_odds%2==0 && total_odds%3!=0)) col_type = "odd-double";

			if(unique_type==2) col_type = "odd-double";
			if(unique_type==3) col_type = "odd-triple";

			if(col_type == "odd-double") {

				for(j in odds) {
					if(j<(total_odds/2)) frst_col.push(odds[j]);
					else scnd_col.push(odds[j]);
				}

			} else {

				for(j in odds) {
					if(j<(total_odds/3)) {
						frst_col.push(odds[j]);
					} else {
						if(j<((total_odds/3)*2)) {
							scnd_col.push(odds[j]);
						} else {
							thrd_col.push(odds[j]);
						}
					}
				}				

			}

			outcome_titles = outcome_titles + e.pid + '.' + market_id + ",";

			var d = '<div class="mt5 market-type market-type-'+market_id+'" data="'+market_id+'">' +
					'	<a href="javascript:;" class="title box-title-action" data-box="market-box-'+market_id+'"><span class="fa fa-caret-right"></span> <span class="market-name mn-'+e.pid+'-'+market_id+'"><!--outcome name--></span></a>' +
					'	<div class="odd-container market-box-'+market_id+'">';

			var max = frst_col.length > scnd_col.length ? frst_col.length : scnd_col.length;
			max = thrd_col.length > max ? thrd_col.length : max;

			if(max>0) {
				for(j=0; j<max; j++) {
					for(x=0; x<3; x++) {

						var odd = null;

						if(x==0 && frst_col.length>=(j-1)) odd = frst_col[j];
						if(x==1 && scnd_col.length>=(j-1)) odd = scnd_col[j];
						if(x==2 && thrd_col.length>=(j-1)) odd = thrd_col[j];

						if(odd==null) continue;

						if(odd.specifiers!=null && odd.specifiers!="") {
							odd.specifiers = fix_specifiers_name(odd.specifiers);
						}

						var oname = findKeyword(odd.name);
						if(odd.specifiers!=null && odd.specifiers!="" && odd.specifiers!="-1") oname = oname + " " + numberFormat(odd.specifiers);

						d = d + '<a href="javascript:;" class="odd-link odd-sub-button odd-link-'+odd.id+' ' + col_type + '" data="'+e.id+'x'+market_id+'x'+odd.id+'" price="' + odd.odds + '" oname="' + oname + '">';
						d = d + '<div class="odd-title"><span>'+findKeyword(odd.name)+'</span>';

						if(odd.specifiers!=null && odd.specifiers!="" && odd.specifiers!="-1")
						{
							d = d + '<span class="odd-line">'+numberFormat(odd.specifiers)+'</span>';
						}

						d = d + '</div>';
						d = d + '<div class="odd-rate odd-main-button odd-'+odd.id+'">'+oddFormat(odd.odds)+'</div>';
						d = d + '</a>';
					}
				}
			}

			d = d +
				'		<div class="clear"></div>' +
				'	</div>' +
				'</div>';

			$(".market-types").append(d);

			$(".box-title-action").unbind("click");
			$(".box-title-action").click(function() {
				var box = $(this).attr("data-box");
				$("."+box).toggle();
			});			

		}

		$(".market-type").each(function() {
			var c = $(this).find(".odd-link").length;
			if(c==0) $(this).remove();
		});		

		$.post(typeof(link_set)=="object"&&link_set.outcomes!=null?link_set.outcomes:"/api/sport/data/outcomes",{outcomes:outcome_titles,type:$("#subscribe-type").val()},function(response) {
			var json = typeof response == "object" ? response : JSON.parse(response);
			if(json.result==null || json.result!="ok") {
				console.log("reload-place-3");
				location.reload();
				return false;
			}
			if(json.data!=null) {
				for(i in json.data)
				{
					var item = json.data[i];
					var fixed_name = fix_market_name(item.name);
					//$(".market-type-" + item.id + " .market-name").html(fixed_name);
					$(".mn-" + item.pid + "-" + item.id).html(fixed_name);

					$(".market-type-" + item.id).attr({"data-set":"true"});
				}

				$(".market-type").each(function() {
					var did = $(this).attr("data");
					var d = $(this).attr("data-set");
					if(d==undefined || d!="true") {
						$(".market-type-" + did).remove();
					}
				});
			}
		});

	}

	setEventTime = function(event_id, event_info, event_stats) {

		var event_status = event_info!=null&&event_info.status!=null?event_info.status:"";

		var time_found = false;

		if(event_info!=null && event_info.clock!=null) {
			var lst = event_info.lcu!=null?event_info.lcu:0;
			if(event_info.clock!=null && event_info.clock.stopped=="true") lst = 0;


			var max_extra = 0;
			if(event_info.clock!=null && event_info.clock.stoppage_time!=null || event_info.clock.stoppage_time_announced!=null) {
				if(event_info.clock.stoppage_time!=null) {
					var me = event_info.clock.stoppage_time.split(":");
					if(me.length==2 && me[1].length==2) {
						max_extra = parseInt(me[0])*60+parseInt(me[1]);
					}
				}
				if(event_info.clock.stoppage_time_announced!=null) {
					var me = event_info.clock.stoppage_time_announced.split(":");
					if(me.length==2 && me[1].length==2) {
						max_extra = parseInt(me[0])*60+parseInt(me[1]);
					}
				}				


			}

			if(!time_found && event_info.clock!=null && event_info.clock.remaining_time!=null) {
				var me = event_info.clock.remaining_time.split(":");
				if(me.length==2 && me[1].length==2) {
					time_found = true;
					var time = parseInt(me[0])*60+parseInt(me[1]);
					$(".event-"+event_id+" .event-minute").html(timeFormat(event_info.clock.remaining_time));
					$(".event-"+event_id+" .event-minute").attr({"last-time":lst,"step":-1,"time":time,"me":max_extra});
				}
			}
			if(!time_found && event_info.clock!=null && event_info.clock.remaining_time_in_period!=null) {
				var me = event_info.clock.remaining_time_in_period.split(":");
				if(me.length==2 && me[1].length==2) {
					time_found = true;
					var time = parseInt(me[0])*60+parseInt(me[1]);
					$(".event-"+event_id+" .event-minute").html(timeFormat(event_info.clock.remaining_time_in_period));
					$(".event-"+event_id+" .event-minute").attr({"last-time":lst,"step":-1,"time":time,"me":max_extra});
				}
			}
			if(!time_found && event_info.clock!=null && event_info.clock.match_time!=null) {
				var me = event_info.clock.match_time.split(":");
				if(me.length==2 && me[1].length==2) {
					time_found = true;
					var time = parseInt(me[0])*60+parseInt(me[1]);
					$(".event-"+event_id+" .event-minute").html(timeFormat(event_info.clock.match_time));
					$(".event-"+event_id+" .event-minute").attr({"last-time":lst,"step":1,"time":time,"me":max_extra});
				}
			}			
			if(!time_found && event_status!="") {
				//time_found = true;
				$(".event-"+event_id+" .event-minute").html(findKeyword(event_status));
				$(".event-"+event_id+" .event-minute").attr({"last-time":0,"me":max_extra});	
			}
		}

		

		return time_found;

	}

	get_score = function(event_info, event_stats) {

		if(event_info!=null && event_info.scores!=null && event_info.scores.home_score!=null && event_info.scores.away_score!=null)
		{
			return {"home":event_info.scores.home_score,"away":event_info.scores.away_score};
		}

		if(event_stats!=null && event_stats.status!=null && event_stats.status.home_score!=null && event_stats.status.away_score!=null)
		{
			return {"home":event_stats.status.home_score,"away":event_stats.status.away_score};
		}
		
		return {"home":"&nbsp;","away":"&nbsp;"};

	}

	get_full_score = function(event_info, event_stats) {

		var result = {};

		if(event_info!=null && event_info.scores!=null && event_info.scores.home_score!=null && event_info.scores.away_score!=null)
		{
			result["0"] = {"home":event_info.scores.home_score,"away":event_info.scores.away_score};
		}

		if(event_stats!=null && event_stats.status!=null && event_stats.status.home_score!=null && event_stats.status.away_score!=null)
		{
			result["0"] = {"home":event_stats.status.home_score,"away":event_stats.status.away_score};
		}

		if(event_stats===null || event_stats.period_scores==null) {
			return result;
		}

		for(i in event_stats.period_scores) {
			var ps = event_stats.period_scores[i];
			result[""+ps.number] = {"home":ps.home_score,"away":ps.away_score};
		}

		return result;

	}

	get_stats = function(event_info, event_stats) {

		var r = {"home":{"red":0,"yellow":0},"away":{"red":0,"yellow":0}};

		if(event_stats!=null && event_stats!=null && event_stats.statistics!=null) {
			if(event_stats.statistics.red_cards!=null && event_stats.statistics.red_cards.home!=null)	r.home.red = parseInt(event_stats.statistics.red_cards.home);
			if(event_stats.statistics.red_cards!=null && event_stats.statistics.red_cards.away!=null)	r.away.red = parseInt(event_stats.statistics.red_cards.away);
			if(event_stats.statistics.yellow_cards!=null && event_stats.statistics.yellow_cards.home!=null)	r.home.yellow = parseInt(event_stats.statistics.yellow_cards.home);
			if(event_stats.statistics.yellow_cards!=null && event_stats.statistics.yellow_cards.away!=null)	r.away.yellow = parseInt(event_stats.statistics.yellow_cards.away);
		}

		return r;

	}

	add_odd_effect = function(item, color) {

		if($(item).hasClass("passive") || $(item).hasClass("passive-ev") || $(item).hasClass("passive-ma")) return false;
		if($(item).hasClass("passive-link") || $(item).hasClass("passive-link-ev") || $(item).hasClass("passive-link-ma")) return false;

		$(item).addClass(color+"-back");
		setTimeout(function() {
			$(item).removeClass(color+"-back");
			setTimeout(function() {
				$(item).addClass(color+"-back");
				setTimeout(function() {
					$(item).removeClass(color+"-back");
					setTimeout(function() {
						$(item).addClass(color+"-back");
						setTimeout(function() {
							$(item).removeClass(color+"-back");
							setTimeout(function() {
								$(item).addClass(color+"-back");
								setTimeout(function() {
									$(item).removeClass(color+"-back");
								}, 250);
							}, 250);
						}, 250);
					}, 250);
				}, 250);
			}, 250);
		}, 250);
	}

	var dest;

	/*
	var pmasian = [51,52,53,54,260,261,473,482,562,566];
	var liasian = [33,34,35,36,38,44,48,51,54,76,113,114,349,351,371,373,375,377,379,381,383,385,387,389,391,393,395,482,528,890,892,930,1046,1094,1142,1234,1264,1266,1268,1270,1272,1274,1290,1308,1368,1396,1406,1428,1442,1456,1514,1630,1634];
	*/

	sortOutcomes = function(oc,pid) {

		// Fixing asian handicap signs
		/*
		if(pid==1) {
			for(i in oc) {
				if(oc[i].outcomes.length==0) continue;
				var prmt = oc[i].outcomes[0].type=="prematch";
				if(prmt && !inArray(oc[i].id,pmasian)) continue;
				if(!prmt && !inArray(oc[i].id,liasian)) continue;
				for(j in oc[i].outcomes)
				{
					if(oc[i].outcomes[j].name=="1") {
						if(oc[i].outcomes[j].specifiers.length>0 && oc[i].outcomes[j].specifiers[0]!='-' && oc[i].outcomes[j].specifiers[0]!='+') {
							oc[i].outcomes[j].specifiers = "+" + oc[i].outcomes[j].specifiers;
						}
					} else if(oc[i].outcomes[j].name=="2") {
						if(oc[i].outcomes[j].specifiers.length==0) {
							continue;
						} else {
							if(oc[i].outcomes[j].specifiers[0]=='-') {
								oc[i].outcomes[j].specifiers = '+' + oc[i].outcomes[j].specifiers.substr(1);
							} else if(oc[i].outcomes[j].specifiers[0]=='+') {
								oc[i].outcomes[j].specifiers = '-' + oc[i].outcomes[j].specifiers.substr(1);
							} else {
								oc[i].outcomes[j].specifiers = "-" + oc[i].outcomes[j].specifiers;
							}
						}
					}
				}
			}
		}
		*/

		dest = oc;

		var r = [];
		
		for(jid in market_priority) {
			for(i in oc) {
				if(pid+"@"+oc[i].id==market_priority[jid]) {
					oc[i].outcomes = oc[i].outcomes.sort(compareByTeamBet);
					r.push(oc[i]);
					break;
				}
			}
		}

		for(i in oc) {
			if(!inArray2(oc[i].id,market_priority,pid)) {
				if(inArray2(oc[i].id,team_name_markets,pid)) {
					oc[i].outcomes = oc[i].outcomes.sort(compareByTeamBet);
				} else {
					oc[i].outcomes = oc[i].outcomes.sort(compareByBet);
				}
				r.push(oc[i]);
			}
		}

		/*
		var home_team_name = $(".home-team").html();
		if(home_team_name==undefined || home_team_name==null) home_team_name = "1";

		var away_team_name = $(".away-team").html();
		if(away_team_name==undefined || away_team_name==null) away_team_name = "2";
		*/

		var home_team_name = findKeyword("home");
		var away_team_name = findKeyword("away");

		// Replace with team names
		for(i in r) {
			if(inArray2(r[i].id,team_name_markets,pid)) {
				for(j in r[i].odds) {
					if(r[i].odds[j].name=="1") {
						r[i].odds[j].name = home_team_name;
					} else if(r[i].odds[j].name=="2") {
						r[i].odds[j].name = away_team_name;
					}
				}
			}
		}

		return r;

	}

	inArray = function(xId,xArray) {
		for(ij in xArray) if(xArray[ij]==xId) return true;
		return false;
	}

	inArray2 = function(xId,xArray,xPid) {
		for(ij in xArray)
		{
			if(xArray[ij]==xPid+"@"+xId) return true;
		}
		return false;
	}

	compareByBet = function(a,b) {

		if(a.name < b.name) {
			return -1;
	 	}

		if(a.name > b.name) {
			return 1;
		}

		if(a.specifiers < b.specifiers) {
			return -1;
		}

		if(a.specifiers > b.specifiers) {
			return 1;
		}

		if(a.most_balanced!=null || b.most_balanced!=null) {
			if(mostBalancedCode(a) < mostBalancedCode(b)) return -1;
			if(mostBalancedCode(a) > mostBalancedCode(b)) return 1;
		}

		return 0;
	}

	compareByTeamBet = function(a,b) {

		if(teamNameCode(a) < teamNameCode(b)) return -1;
		if(teamNameCode(a) > teamNameCode(b)) return 1;

		if(a.specifiers > b.specifiers) {
			return 1;
		}

		if(a.specifiers < b.specifiers) {
			return -1;
		}

		if(a.most_balanced!=null || b.most_balanced!=null) {
			if(mostBalancedCode(a) < mostBalancedCode(b)) return -1;
			if(mostBalancedCode(a) > mostBalancedCode(b)) return 1;
		}

		return 0;
	}

	mostBalancedCode = function(o) {
		if(o.most_balanced!=null) {
			if(o.most_balanced=='Yes') return -1;
			if(o.most_balanced=='No') return 1;
		}
		return 0;
	}

	teamNameCode = function(o) {
		if(o.name!=null) {
			if(o.name=='1') return -1;
			if(o.name=='2') return 1;
		}
		return 0;
	}

	calculateTimes = function() {

		$(".event-minute").each(function() {
			var step = $(this).attr("step");
			var last = $(this).attr("last-time");
			var time = $(this).attr("time");
			var max_extra = $(this).attr("me");
			if(step==undefined || last==undefined || time==undefined || last==0) return true;
			var dif = new Date().getTime()-parseInt(last);

			if(max_extra!=undefined && max_extra>0 && step>0) {
				max_extra = max_extra * 1000;
				if(dif>max_extra) dif = max_extra;
			}

			time  = parseInt(time);
			if(step>0) time += Math.floor(dif / 1000);
			if(step<0) time -= Math.floor(dif / 1000);
			if(time<0) time  = 0;
			var second = time % 60;
			var minute = Math.floor(time / 60);
			if(second<10) second = "0"+second;
			$(this).html(timeFormat(minute+":"+second));
		});

	}

	fix_market_name = function(nm) {

		var name = nm.replace(" {hcp}","");
		name = name.replace("{$competitor1}","Home");
		name = name.replace("{$competitor2}","Away");
		name = name.replace("- {!goalnr} goal","- Goal count");
		name = name.replace("{!goalnr} ","");
		name = name.replace(" [{score}]","");
		name = name.replace("{!quarternr} ","");
		name = name.replace("{!setnr} ","");
		name = name.replace("{!inningnr} ","");
		name = name.replace("{!cornernr} ","");
		
		return name;

	};

	fix_outcome_name = function(nm) {

		var name = nm.replace("{$competitor1} ","1");
		name = name.replace("{$competitor2} ","2");
		name = name.replace("{$competitor1}","1");
		name = name.replace("{$competitor2}","2");
		name = name.replace("{$competitor1}","1");
		name = name.replace("{$competitor2}","2");
		name = name.replace("draw","x");
		name = name.replace(" {total}","");
		name = name.replace(" ({hcp})","");
		name = name.replace(" ({-hcp})","");
		name = name.replace(" ({+hcp})","");
		name = name.replace("{total}","");
		name = name.replace("({hcp})","");
		name = name.replace("({-hcp})","");
		name = name.replace("({+hcp})","");

		return name;

	};

	fix_specifiers_name = function(nm) {

		var name = nm.replace("hcp=","");
		name = name.replace("total=","");
		name = name.replace("score=","");
		name = name.replace("goalnr=","");
		name = name.replace("variant=sr:point_range:","");
		name = name.replace("variant=sr:winning_margin:","");
		name = name.replace("quarternr=",""); // quarternr=1|-2.5
		name = name.replace("setnr=","");
		name = name.replace("inningnr=","");
		name = name.replace("cornernr=","");
		
		return name;		

	};

	function getconnectionurl(current_url,cannotconnect) {
		if(cannotconnect==true) {
			var cck = getcookieforconnection("connfix");
			if(cck!=null) {
				var cck = parseInt(cck);
				cck = cck +1;
				if(cck>=4) {
					createcookieforconnection("connfix",1);					
					return current_url;	
				}
				createcookieforconnection("connfix",cck);
				return current_url;
			}
			createcookieforconnection("connfix",1);
			return current_url;
		}
		var cck = getcookieforconnection("connfix");
		if(cck!=null && parseInt(cck)>=2) {
			//return current_url;
			return current_url.replace("ws://","wss://");
		}
		return current_url;
	}

	function createcookieforconnection(name,value) {
		var date = new Date();
		date.setTime(date.getTime()+(6*60*60*1000));
		var expires = "; expires="+date.toUTCString();
	    document.cookie = name+"="+value+expires+"; path=/";
	}

	function getcookieforconnection(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	}




